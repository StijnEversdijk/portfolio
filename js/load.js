$(document).ready( function()
	{
		$('#landing h1').hide();
		$('#landing h2').hide();
		$('#landing p').hide();
		$('#landing a').hide(0, showNaam);	
	});
	
	function showNaam()
		{
			$('#landing h1').fadeIn(1000, showFunctie);
		}
		
	function showFunctie()
		{
			$('#landing h2').fadeIn(800, showTekst);
		}
		
	function showTekst()
		{
			$('#landing p').fadeIn(600);
			$('#landing a').fadeIn(600);
		}
		