$(window).scroll(function() {
    if ($(window).scrollTop() > 800) {
       $('#FixedNavi').fadeIn(800, showSubprojecten); // > 100px from top - show div
    }
    else {
        $('#FixedNavi').fadeOut(400);// <= 100px from top - hide div
		$('#projecten-sub').fadeOut(100);
    }
});

function showSubprojecten()
	{
		$('#projecten-sub').fadeIn();
	}
	
function hideSubprojecten()
	{
		$('#projecten-sub').fadeOut(100);
	}
	